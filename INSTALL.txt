
Installation
------------
Copy jsxgraphdrupal directory to your modules directory and then enable on the admin modules page.

Retrieve the latest jsxgraph scripts and css from the URL below:
http://jsxgraph.uni-bayreuth.de/distrib/jsxgraphcore.js
http://jsxgraph.uni-bayreuth.de/distrib/jsxgraph.css

Save the script in the jsxgraphdrupal directory.

The install file will create the appropriate filter and input filters. However one still need to configure the JSXGraph to add the enable the JSXGraph filter (and add save filters like HTML Filter etc).

I repeat once you have installed the module, you still need to configure the JSXGraph input format under 
Administrater->Input Formats->Input Filters 

