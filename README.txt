
JSXGraph wrapper module for Drupal. It renders the JSXGraph tags and adds the appropriate JS and CSS.

Installation
------------
Copy jsxgraphdrupal directory to your modules directory and then enable on the admin modules page.
The install file will create the appropriate filter and input filters. However one still need to configure the JSXGraph to add the enable the JSXGraph filter (and add save filters like HTML Filter etc).

I repeat once you have installed the module, you still need to configure the JSXGraph input format under 
Administrater->Input Formats->Input Filters 

Latest JSXGraph scripts:
------------------------
Can be retrieved here:
http://jsxgraph.uni-bayreuth.de/distrib/jsxgraphcore.js
http://jsxgraph.uni-bayreuth.de/distrib/jsxgraph.css

Cautions
-------
The text within <jsxgraph /> tags counts as characters when Drupal calculates the teaser. It will truncate the code (Drupal takes first 200 characters for teaser). 
I suggest use TeaserbyTyte or htmLawed to filter the <jsxgraph /> tags.

Teaserbytype: http://drupal.org/project/teaserbytype
htmLawed: http://drupal.org/project/htmLawed

Acknowledgement
---------------
The module is based on other plugins Moodle / Wordpress that are listed at http://jsxgraph.uni-bayreuth.de/wp/

Author
------
Godwin Chan
GodwinCodes@gmail.com
